package com.lfds.ws.soap;

import org.apache.cxf.feature.Features;

import com.jaom.ws.trainings.CustomerOrdersPortType;
import com.jaom.ws.trainings.UserLoginRequest;
import com.jaom.ws.trainings.UserLoginResponse;


@Features(features = "org.apache.cxf.feature.LoggingFeature")
public class CustomerOrdersWSImpl implements CustomerOrdersPortType{

	@Override
	public UserLoginResponse userLogin(UserLoginRequest parameters) {
		
		String passRequest = parameters.getPass();
		String userRequest = parameters.getUser();
		boolean respuesta = false;
		
		
		UserLoginResponse response = new UserLoginResponse();
		
		if(userRequest.equals("userChido") && passRequest.equals("passChido")) respuesta = true;
	
		response.setResult(respuesta);
		
		return response;
	}


}
